import json


class OverpassError(Exception):
    def __init__(self, msg, error_code):
        self.msg = msg
        self.error_code = error_code
        super().__init__()


def convert_to_geojson(nodes):
    features = []

    for node_id, node in nodes.items():
        feature = {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [node["longitude"], node["latitude"]],
            },
            "properties": {
                "id": node["id"],
                "osm_id": node["osm_id"],
                "local_street": node["local_street"],
                "transit_street": node["transit_street"],
                "transit_node": node["transit_node"],
                "local_street_double_way": node["local_street_double_way"],
                "local_street_modal_filter": node["local_street_modal_filter"],
            },
        }
        features.append(feature)

    geojson = {
        "type": "FeatureCollection",
        "features": features,
    }

    return geojson


def compute_nodes(overpass_data, transit_ways="primary,secondary"):
    data = overpass_data
    nodes = {}

    ways = [el for el in data["elements"] if el["type"] == "way"]

    for el in data["elements"]:
        if el["type"] == "node":
            node = {
                "longitude": el["lon"],
                "latitude": el["lat"],
                "id": str(el["id"]),
                "local_street": [],
                "transit_street": [],
                "transit_node": None,
                "local_street_double_way": [],
                "local_street_modal_filter": [],
                "transit_name": "",
            }
            nodes[el["id"]] = node

    # identify transit nodes
    transit_nodes = []
    for el in ways:
        if el["tags"].get("highway", "tertiary") in transit_ways:
            for node_id in el["nodes"]:
                if node_id in nodes:
                    if not nodes[node_id]["transit_node"]:
                        nodes[node_id]["transit_node"] = "1"
                        nodes[node_id]["transit_name"] = el["tags"].get(
                            "name", "unkown"
                        )

    # join disjoint ways
    for i, seg1 in enumerate(ways):
        for j, seg2 in enumerate(ways):
            if i == j:
                continue
            if seg1["nodes"] and seg2["nodes"]:
                if (
                    seg1["nodes"][-1] == seg2["nodes"][0]
                    and seg1["nodes"][-1] not in nodes
                ):
                    # tail to head
                    seg1["nodes"].extend(seg2["nodes"])
                    seg2["nodes"] = []
                elif (
                    seg1["nodes"][0] == seg2["nodes"][-1]
                    and seg1["nodes"][0] not in nodes
                ):
                    # head to tail
                    seg2["nodes"].extend(seg1["nodes"])
                    seg1["nodes"] = []
                elif (
                    seg1["nodes"][0] == seg2["nodes"][0]
                    and seg1["nodes"][0] not in nodes
                ):
                    # head to head, inverse first and append second
                    seg2["nodes"] = seg1["nodes"][::-1] + seg2["nodes"][1:]
                    seg1["nodes"] = []
                elif (
                    seg1["nodes"][-1] == seg2["nodes"][-1]
                    and seg1["nodes"][-1] not in nodes
                ):
                    # tail to tail, reverse second
                    seg1["nodes"].extend(seg2["nodes"][::-1])
                    seg2["nodes"] = []

    # add nodes to street lists
    for el in ways:
        way_nodes = el["nodes"]
        # use only known nodes
        way_nodes = list(filter(lambda n: n in nodes, way_nodes))
        for start_node, end_node in zip(way_nodes[:-1], way_nodes[1:]):
            if el["tags"].get("highway", "tertiary") in transit_ways:
                # make sure that the transit node is not assigned to two different
                # transit axes
                if (
                    "name" in el["tags"]
                    and nodes[start_node]["transit_name"] == el["tags"]["name"]
                ):
                    street_type = "transit_street"
                else:
                    street_type = None
            else:
                street_type = (
                    "local_street"
                    if el["tags"].get("oneway", "no") == "yes"
                    else "local_street_double_way"
                )
            if street_type:
                if (
                    end_node not in nodes[start_node][street_type]
                    and start_node not in nodes[end_node][street_type]
                ):
                    nodes[start_node][street_type].append(end_node)

    # replace ids

    sorted_features = sorted(
        nodes.items(), key=lambda x: (-x[1]["latitude"], x[1]["longitude"])
    )
    node_mapping = {node_id: i + 1 for i, (node_id, _) in enumerate(sorted_features)}

    def replace_id(ids: list[int]):
        return [node_mapping[i] for i in ids]

    connection_attrs = [
        "local_street",
        "transit_street",
        "local_street_double_way",
        "local_street_modal_filter",
    ]

    nodes = {
        node_mapping[node_id]: (
            node
            | {
                connection_name: replace_id(node[connection_name])
                for connection_name in connection_attrs
            }
            | {"id": str(node_mapping[node_id]), "osm_id": node["id"]}
        )
        for node_id, node in nodes.items()
    }

    # convert street lists to strings
    for el in nodes.values():
        for col in connection_attrs:
            el[col] = ",".join(map(str, el[col]))

    return nodes


import requests


def get_overpass_data(bounding_box=None, poly=None):

    with open("overpass-query.txt", "r") as fid:
        overpass_query = fid.read()
    # Define the Overpass API URL
    overpass_url = "https://overpass-api.de/api/interpreter"

    if bounding_box is not None:
        selection = bounding_box
    elif poly is not None:
        selection = f'poly: "{poly}"'
    else:
        raise TypeError("Need to specify either bounding_box or poly")

    # Create a dictionary with the query and bounding box
    data = overpass_query.replace("{{ selection }}", selection)

    # Send an HTTP POST request to the Overpass API
    response = requests.post(overpass_url, data=data)

    if response.status_code == 200:
        # If the request is successful, return the response content
        return response.json()
    else:
        # If the request fails, print an error message and raise an exception
        print(f"Error: {response.status_code} - {response.reason}")
        raise OverpassError(msg=response.text, error_code=response.status_code)


def geojson_from_bbox(bounding_box):

    data = get_overpass_data(bounding_box)

    nodes = compute_nodes(data)

    geojson = convert_to_geojson(nodes)


if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        with open(sys.argv[1]) as fid:
            data = json.load(fid)
    else:
        bounding_box = (
            "48.82124778199317,2.342770099639893,48.82428520010334,2.348434925079346"
        )
        data = get_overpass_data(bounding_box=bounding_box)

    nodes = compute_nodes(data)

    geojson = convert_to_geojson(nodes)
    with open("converted.json", "w") as fid:
        json.dump(geojson, fid, indent=2)

    poly = "48.82743544393984 2.33236312866211 48.821417317156765 2.3311185836791997 48.822801832861295 2.3249816894531254"
    data = get_overpass_data(poly=poly)

    nodes = compute_nodes(data)

    geojson = convert_to_geojson(nodes)
    with open("converted_poly.json", "w") as fid:
        json.dump(geojson, fid, indent=2)
