import json
from flask import Flask, request, Response
from convert_geojson import (
    get_overpass_data,
    compute_nodes,
    convert_to_geojson,
    OverpassError,
)

app = Flask(__name__)
DEBUG = False


def geojson_from_bbox(**kwargs):

    data = get_overpass_data(**kwargs)

    if DEBUG:
        # debugging
        with open("overpass-debug.json", "w") as fid:
            json.dump(data, fid)

    nodes = compute_nodes(data)

    geojson = convert_to_geojson(nodes)
    return geojson


def send_geojson(content, filename="quartier.json"):
    return Response(
        json.dumps(content),
        mimetype="application/json",
        headers={"Content-Disposition": f"attachment;filename={filename}"},
    )


@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"


@app.route("/get_data/", methods=["GET"])
def get_data():
    bounding_box = request.args.get("bbox")
    poly = request.args.get("poly")

    try:
        if bounding_box:
            geojson = geojson_from_bbox(bounding_box=bounding_box)
            return send_geojson(geojson)
        elif poly:
            geojson = geojson_from_bbox(poly=poly)
            return send_geojson(geojson)
        else:
            return "could not understand request", 400
    except OverpassError as exc:
        return exc.msg, exc.error_code
