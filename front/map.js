// Initialize Leaflet map
const map = L.map('map', {doubleClickZoom: false}).setView([48.8566, 2.3522], 12);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

// Initialize variables for bounding box
let startPoint = null;
let endPoint = null;
let selectionBox = null;
let bbox = null;

const backend_url = 'https://q922f5u002.execute-api.us-east-1.amazonaws.com/get_data/';
// const backend_url = 'http://127.0.0.1:5000/get_data/';


// Function to update the selection box during drag
function updateSelectionBox() {
    if (startPoint && endPoint) {
        const bounds = [
            [Math.min(startPoint.lat, endPoint.lat), Math.min(startPoint.lng, endPoint.lng)],
            [Math.max(startPoint.lat, endPoint.lat), Math.max(startPoint.lng, endPoint.lng)]
        ];
        
        if (!selectionBox) {
            selectionBox = L.rectangle(bounds, { color: 'blue', weight: 2 }).addTo(map);
        } else {
            selectionBox.setBounds(bounds);
            bbox = bounds[0][0] + ',' + bounds[0][1] + ',' + bounds[1][0]+ ',' + bounds[1][1];
            console.log("Bounding Box Coordinates:", bbox);
        }
    }
}

// Function to handle map click events
function handleMapClick(e) {
    if (!startPoint) {
        startPoint = e.latlng;
        // L.marker(startPoint).addTo(map);
    } else if (!endPoint) {
        endPoint = e.latlng;
        // L.marker(endPoint).addTo(map);
        updateSelectionBox();
    }
    else {
        // reset and start ne selection
        startPoint = e.latlng;
        endPoint = null;
        selectionBox.setBounds([startPoint, startPoint]);
    }
}

// Attach click event listener to the map
// map.on('click', handleMapClick);
// Handle drag interaction
map.on('mousemove', function (e) {
    if (startPoint && !endPoint) {
        if (!selectionBox) {
            selectionBox = L.rectangle([startPoint, e.latlng], { color: 'blue', weight: 2 }).addTo(map);
        } else {
            selectionBox.setBounds([startPoint, e.latlng]);
        }
    }
});


var polygon = L.control.polygon({position: 'topleft'})
polygon.addTo(map);

// Get the button element by its id
const button = document.getElementById('download');

// Add a click event listener to the button
button.addEventListener('click', function() {
    // Replace this with the action you want to perform when the button is clicked
    console.log(poly_str);
    if (polygon.vertices) {
        var poly_str = polygon.vertices.map(c => c.lat + " " + c.lng).reduce((a, b) => a + " " + b);
        const apiUrl = backend_url + '?poly='+poly_str;

        // Redirect the user to the JSON response URL
        window.location.href = apiUrl;

    }
    else alert('Click on the map to define a bounding box');
});

