
function is_close(coord1, coord2) {
    const eps = 0.0001;
    if ((Math.abs(coord1.lat - coord2.lat) < eps) &&  (Math.abs(coord1.lng - coord2.lng)) < eps)
    {
        return true
    };
    return false;
}

L.Control.Polygon = L.Control.extend({
    vertices: [],
    _polygon: null,
    _enabled: false,
    onAdd: function(map) {
        var container = L.DomUtil.create('div', 'leaflet-bar');
        const link = L.DomUtil.create('a', 'polygon-btn', container);
        link.innerHTML = '<img width="100%" src="polygon.svg"/>';
        link.setAttribute('role', 'button');

        L.DomEvent.disableClickPropagation(container);
        L.DomEvent.on(link, 'click', this._enable, this);
        return container;
    },

    onRemove: function(map) {
        // Nothing to do here
    },

    _enable: function(e) {
        if (!this._enabled) {
            this.vertices = [];
            this._map.on("click", this._add_point, this);
            this._map.on("mousemove", this._on_move, this);
            if (this._polygon === null)
                this._polygon = L.polygon(this.vertices).addTo(this._map);
            this._polygon.setStyle({color: 'red', dashArray: '4'});
            this._enabled = true;
            this._map.dragging.disable();
        }  else {
            this._disable();
        };

    },

    _disable: function() {
        this._map.off("click", this._add_point, this);
        this._map.off("mousemove", this._on_move, this);
        this._enabled = false;
        this._map.dragging.enable();
        this._polygon.setStyle({color: 'blue', dashArray: null});

    },

    _add_point: function (e) {
        var n = this.vertices.length;
        if ((n > 2) && (is_close(e.latlng,this.vertices[n-1]))) {

            this._disable();
            this._polygon.setLatLngs(this.vertices);
        }
        else {
            this.vertices.push(e.latlng);
            this._polygon.setLatLngs(this.vertices);
        };
    },
    _on_move: function(e) {
        this._polygon.setLatLngs(this.vertices.concat([e.latlng]));
    },
});

L.control.polygon = function(opts) {
    return new L.Control.Polygon(opts);
}
